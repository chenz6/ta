Columns in the dataset:
(All repetitions are calculated between the current and the previous sentence)
jaccard_sim_char_1_1: repetition similarity of 1-gram character
jaccard_sim_char_2_2: repetition similarity of 2-gram character
jaccard_sim_char_3_3: repetition similarity of 3-gram character
jaccard_sim_word_1_1: repetition similarity of 1-gram word
jaccard_sim_word_2_2: repetition similarity of 2-gram word
jaccard_sim_word_3_3: repetition similarity of 3-gram word

Jaccard similarity:
It is a measure of how similar two sets are.
The Jaccard similarity of two sets is the size of the intersection divided by the size of the union of the two sets.