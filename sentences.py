"""
Process the dataset into sentences and labels
"""
import pandas as pd


def load_data(filepath):
    # Load the dataset from a CSV file
    return pd.read_csv(filepath)


def preprocess_data(data):
    # Determine speaker type based on 'move_id'
    data['speaker'] = data['move_id'].apply(lambda x: 'follower' if 'f' in x else 'giver')

    # Create a flag that marks where the speaker changes
    data['speaker_change'] = data['speaker'].ne(data['speaker'].shift()).cumsum()

    # Group data by this new speaker change indicator to aggregate sentences
    grouped = data.groupby(['speaker_change', 'speaker']).agg({
        'timed_units': lambda x: ' '.join(x.dropna()),
        'move_id': list,
        'label': lambda x: list(x.dropna().unique())
    }).reset_index()

    # Rename columns appropriately
    grouped.rename(columns={'timed_units': 'sentence'}, inplace=True)
    grouped.drop(columns='speaker_change', inplace=True)  # Remove the temporary column used for grouping

    return grouped


def save_data(grouped_data, output_path):
    # Save the processed data to a new CSV file
    grouped_data.to_csv(output_path, index=False)


def process(filepath, output_path):
    data = load_data(filepath)
    grouped_data = preprocess_data(data)
    save_data(grouped_data, output_path)


def main():
    # process all csv files in the dataset folder
    import os
    for filename in os.listdir('./dataset'):
        if filename.endswith(".csv"):
            print(filename)
            process(f'./dataset/{filename}', f'./sentences/{filename}')


if __name__ == '__main__':
    main()
