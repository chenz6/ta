"""
Get the repetition similarity between two consecutive sentences in a text.
"""
import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import jaccard_score


def calculate_jaccard_similarity(text1, text2, ngram_range=(2, 2), analyzer='char'):
    if not text1.strip() or not text2.strip():  # Check if texts are empty or just whitespace
        return 0.0  # Return zero similarity if either text is empty
    vectorizer = CountVectorizer(analyzer=analyzer, ngram_range=ngram_range)
    try:
        X = vectorizer.fit_transform([text1, text2])
        X_binary = X.toarray().astype(bool).astype(int)
        return jaccard_score(X_binary[0], X_binary[1], average='binary')
    except ValueError:
        return 0.0  # Return zero similarity if an empty vocabulary error occurs


def load_data(filepath):
    return pd.read_csv(filepath)


def preprocess_data(data):
    # Remove rows with NaN in 'sentence' to ensure clean text analysis
    data = data.dropna(subset=['sentence'])
    data['previous_sentence'] = data['sentence'].shift(1)
    return data


def add_jaccard_similarity(data, ngram_range=(2, 2), analyzer='char'):
    # Initialize a column for Jaccard similarity
    sim_name = f'jaccard_sim_{analyzer}_{ngram_range[0]}_{ngram_range[1]}'
    data[sim_name] = np.nan

    # Calculate Jaccard similarity for each consecutive pair of sentences
    for index, row in data.iterrows():
        if pd.notna(row['previous_sentence']):
            similarity = calculate_jaccard_similarity(row['previous_sentence'], row['sentence'], ngram_range, analyzer)
            data.loc[index, sim_name] = similarity
    return data


def save_data(data, output_path):
    data.to_csv(output_path, index=False)


def process(filepath, output_path):
    data = load_data(filepath)
    data = preprocess_data(data)
    for analyzer in ['char', 'word']:
        for ngram_range in [(1, 1), (2, 2), (3, 3)]:
            data = add_jaccard_similarity(data, ngram_range=ngram_range, analyzer=analyzer)
    data.drop(columns='previous_sentence', inplace=True)
    save_data(data, output_path)


def main():
    # process all csv files in the dataset folder
    import os
    for filename in os.listdir('./sentences'):
        if filename.endswith(".csv"):
            print(filename)
            process(f'./sentences/{filename}', f'./repetitions/{filename}')


if __name__ == '__main__':
    main()
